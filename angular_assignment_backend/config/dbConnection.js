const mysql=require("mysql2");
require('dotenv').config();

const pool=mysql.createPool({
    host:"localhost",
    port:3306,
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    connectionLimit:100,
    database:"angular_assignment_db",
    multipleStatements:true,
    insecureAuth : true
})
module.exports=pool;