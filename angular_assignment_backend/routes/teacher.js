let express = require('express');
let router = express.Router();
const auth = require("../middleware/auth")
const Controller = require("../controller/teacher")

router.post("/teacherLogin", Controller.teacherLogin);

router.post("/saveTeacher", Controller.saveTeacher);

module.exports = router;