import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';

import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-edit-student-dialog',
  templateUrl: './edit-student-dialog.component.html',
  styleUrls: ['./edit-student-dialog.component.css']
})

export class EditStudentDialogComponent implements OnInit {

  studentName: string = "hello"
  chemistry: number = 21;
  maths: number = 92;
  physics: number = 82;
  studentId: number =29;
  studentRollNo: string = "8282882";
  dob: string ="2882";
  isSuccess: any;

  constructor(public dialogRef: MatDialogRef<EditStudentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private studentService : StudentService) {
  }

  ngOnInit(): void {
    if(this.data){
      this.studentName = this.data.studentData.studentName
      this.studentId = this.data.studentData.studentId;
      this.studentRollNo = this.data.studentData.studentRollNo;
      this.dob = this.data.studentData.dob;
      this.physics = this.data.studentData.physics;
      this.maths = this.data.studentData.maths;
      this.chemistry = this.data.studentData.chemistry;
    }
  }

  editStudent(editData: any) {
    if(editData.studentRollNo && editData.studentName && editData.dob && editData.physics && editData.maths && editData.chemistry){
      this.studentService.editStudent(editData, this.studentId).subscribe((data)=>{
        this.isSuccess = data;
        if(this.isSuccess.success){
          Swal.fire("success", "student updated sccessfully", "success");
        }
      })
    }
    this.dialogRef.close();
  }

}
