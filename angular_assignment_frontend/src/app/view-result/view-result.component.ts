import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-result',
  templateUrl: './view-result.component.html',
  styleUrls: ['./view-result.component.css']
})
export class ViewResultComponent implements OnInit {

  studentName : string = "";
  physics : number = 0;
  chemistry : number = 0;
  maths : number = 0;
  percentage : number = 0;

  constructor() { 
    const studentData = JSON.parse( sessionStorage.getItem("studentInfo") + "");
    if(studentData){
      this.studentName = studentData.studentName;
      this.maths = studentData.maths;
      this.physics = studentData.physics;
      this.chemistry = studentData.chemistry;
      this.percentage = Math.round((studentData.physics + studentData.maths + studentData.chemistry) / 3)
    }
  }

  ngOnInit(): void {
  }

}
