import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  date : string
  constructor() {
    let date = new Date().toString().split(" ");
    this.date = `${date[0]} ${date[1]} ${date[2]} ${date[3]}`;
  }

  ngOnInit(): void {

  }

}
