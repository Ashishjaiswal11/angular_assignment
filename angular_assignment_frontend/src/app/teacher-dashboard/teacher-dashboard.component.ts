import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { AddStudentDialogComponent } from '../add-student-dialog/add-student-dialog.component';
import { EditStudentDialogComponent } from '../edit-student-dialog/edit-student-dialog.component';
import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-teacher-dashboard',
  templateUrl: './teacher-dashboard.component.html',
  styleUrls: ['./teacher-dashboard.component.css']
})

export class TeacherDashboardComponent implements OnInit {

  students: any
  columns: string[]

  constructor(public dialog: MatDialog, private studentData: StudentService) {
    this.columns = ["action", "studentId", "studentName", "studentRollNo", "dob", "physics", "maths", "chemistry", "percentage"]
  }

  ngOnInit(): void {
    this.getAllStudents();
  }

  getAllStudents() {
    this.studentData.getAllStudents().subscribe((data) => {
      this.students = data;
      if (this.students.success) {
        this.students = this.students.data;
      }
    })
  }

  deleteStudent(studentId: number) {
    this.studentData.deleteStudent(studentId).subscribe((data) => {
      console.log(data);
    })
  }

  openEditDialog(enterAnimationDuration: string, exitAnimationDuration: string, studentData: any): void {
    let dialogRef = this.dialog.open(EditStudentDialogComponent, {
      width: '250px',
      enterAnimationDuration,
      exitAnimationDuration,
      data: {studentData}
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.getAllStudents();
    })
  }

  openDialog(enterAnimationDuration: string, exitAnimationDuration: string): void {
    let dialogRef =  this.dialog.open(AddStudentDialogComponent, {
      width: '250px',
      enterAnimationDuration,
      exitAnimationDuration,
    });

    dialogRef.afterClosed().subscribe(()=>{
      this.getAllStudents()
    })
  }

}
